package com.zero2oneit.mall.goods.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zero2oneit.mall.common.bean.goods.GroupRule;
import com.zero2oneit.mall.common.bean.goods.NewcomersRule;
import com.zero2oneit.mall.common.query.goods.NewcomersRuleQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.goods.mapper.NewcomersRuleMapper;
import com.zero2oneit.mall.goods.service.NewcomersRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-06-02
 */
@Service
public class NewcomersRuleServiceImpl extends ServiceImpl<NewcomersRuleMapper, NewcomersRule> implements NewcomersRuleService {

    @Autowired
    private NewcomersRuleMapper newcomersRuleMapper;

    @Override
    public BoostrapDataGrid pageList(NewcomersRuleQueryObject qo) {
        int total = newcomersRuleMapper.selectTotal(qo);
        return new BoostrapDataGrid(total, total ==0 ? Collections.EMPTY_LIST : newcomersRuleMapper.selectAll(qo));
    }

    @Override
    public R status(String id, Integer status) {
        NewcomersRule newcomersRule = new NewcomersRule();
        newcomersRule.setId(Long.valueOf(id));
        if (status==1){
            newcomersRule.setRuleStatus(2);
        }else if (status==2){
            newcomersRule.setRuleStatus(1);
        }
        return newcomersRuleMapper.updateById(newcomersRule)>0?R.ok():R.fail();
    }
}