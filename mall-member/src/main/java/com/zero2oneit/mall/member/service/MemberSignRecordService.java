package com.zero2oneit.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zero2oneit.mall.common.bean.member.MemberSignRecord;
import com.zero2oneit.mall.common.query.member.MemberSignRecordQueryObject;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-13
 */
public interface MemberSignRecordService extends IService<MemberSignRecord> {

    BoostrapDataGrid recordList(MemberSignRecordQueryObject qo);

    BoostrapDataGrid recordLists(MemberSignRecordQueryObject qo);

}

