package com.zero2oneit.mall.member.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zero2oneit.mall.common.bean.member.PrizeInfo;
import com.zero2oneit.mall.common.query.member.PrizeInfoQueryObject;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zero2oneit.mall.member.mapper.PrizeInfoMapper;
import com.zero2oneit.mall.member.service.PrizeInfoService;

import java.util.Collections;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-24
 */
@Service
public class PrizeInfoServiceImpl extends ServiceImpl<PrizeInfoMapper, PrizeInfo> implements PrizeInfoService {

    @Autowired
    private PrizeInfoMapper prizeInfoMapper;

    @Override
    public BoostrapDataGrid pageList(PrizeInfoQueryObject qo) {
        //查询总记录数
        int total = prizeInfoMapper.selectTotal(qo);
        return new BoostrapDataGrid(total, total == 0 ? Collections.EMPTY_LIST : prizeInfoMapper.selectRows(qo));
    }
}