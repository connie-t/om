package com.zero2oneit.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zero2oneit.mall.common.bean.member.PrizeRule;
import com.zero2oneit.mall.common.query.member.PrizeRuleQueryObject;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-24
 */
public interface PrizeRuleService extends IService<PrizeRule> {

    BoostrapDataGrid ruleList(PrizeRuleQueryObject qo);

}

