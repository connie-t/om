package com.zero2oneit.mall.service.member.remote;

import com.zero2oneit.mall.common.annotion.OperateLog;
import com.zero2oneit.mall.common.bean.goods.GoodSaleArea;
import com.zero2oneit.mall.common.bean.member.StationManage;
import com.zero2oneit.mall.common.enums.BusinessType;
import com.zero2oneit.mall.common.query.goods.GoodsCategoryQueryObject;
import com.zero2oneit.mall.common.query.member.CouponRuleQueryObject;
import com.zero2oneit.mall.common.query.member.InfoQueryObject;
import com.zero2oneit.mall.common.query.member.MemberCouponQueryObject;
import com.zero2oneit.mall.common.query.member.StationManageQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.feign.member.CouponRuleFeign;
import com.zero2oneit.mall.feign.member.MemberFeign;
import com.zero2oneit.mall.feign.member.StationManageFeign;
import com.zero2oneit.mall.feign.oss.OssFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

/**
 * Description: 远程调用驿站管理服务
 *
 * @author yjj
 * @date 2021/3/1 10:51
 */
@RestController
@RequestMapping("/remote/member")
public class MemberRemote {

    @Autowired
    private MemberFeign memberFeign;

    @Autowired
    private CouponRuleFeign ruleFeign;

    /**
     * 会员信息管理列表
     * @param qo
     * @return
     */
    @PostMapping("/list")
    public BoostrapDataGrid list(@RequestBody InfoQueryObject qo) {
        return memberFeign.list(qo);
    }

    /**
     * 重置会员登录密码
     * @param memberId
     * @param type
     * @return
     */
    @PostMapping("/resetPwd")
    public R resetPwd(@RequestParam("memberId") String memberId, @RequestParam("type") String type){
        return memberFeign.resetPwd(memberId, type);
    }

    /**
     * 禁用/撤销禁用
     * @param memberId
     * @param type
     * @return
     */
    @PostMapping("/status")
    public R status(@RequestParam("memberId") String memberId, @RequestParam("type") String type){
        return memberFeign.status(memberId, type);
    }

    /**
     * 赠送优惠券
     * @param memberIds
     * @param couponId
     * @return
     */
    @PostMapping("/coupon")
    public R coupon(@RequestParam("memberIds") String memberIds, @RequestParam("couponId") String couponId){
        return memberFeign.coupon(memberIds, couponId);
    }

    /**
     * 加载会员优惠券列表
     * @param qo 参数
     * @return
     */
    @PostMapping("/coupon/list")
    public BoostrapDataGrid list(@RequestBody MemberCouponQueryObject qo){
        return memberFeign.pageList(qo);
    }

}
